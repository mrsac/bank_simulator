package com.attinad.project1.bank;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class Crediting implements Transaction {
	public void executeTrans(long amt, long ac, String fromac) {
		SqlCon c = new SqlCon();
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the date ddmmyyyy");
		int datee = sc.nextInt();

		try {
			PreparedStatement stmt = c.con
					.prepareStatement("insert into transaction (acno,amt,type,fromac,tdate) values(?,?,?,?,?)");
			stmt.setLong(1, ac);
			stmt.setLong(2, amt);
			stmt.setString(3, "C");
			stmt.setString(4, fromac);
			stmt.setInt(5, datee);
			int i = stmt.executeUpdate();
			stmt = c.con.prepareStatement("select balance from ac_info where ac_no = ?");
			stmt.setLong(1, ac);
			ResultSet rs = stmt.executeQuery();
			long balance = 0;
			while (rs.next()) {

				balance = rs.getLong(1) + amt;
			}
			stmt = c.con.prepareStatement("update ac_info  set balance = ? where ac_no = ?");
			stmt.setLong(1, balance);
			stmt.setLong(2, ac);
			i = stmt.executeUpdate();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
