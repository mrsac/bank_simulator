package com.attinad.project1.bank;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Report {
	SqlCon c = new SqlCon();
	PreparedStatement stmt;

	void dayCheck(int tdate) {

		try {
			stmt = c.con.prepareStatement(
					"select count(amt),acno from transaction where tdate=? and type = 'C' group by acno");
			stmt.setInt(1, tdate);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				if (rs.getInt(1) >= 10) {
					System.out.println("Suspicious Ac Spotted Ac no : " + rs.getLong(2));
					System.out.println("for performing  : " + rs.getInt(1) + " transactions");
				}
			}

		} catch (SQLException e) {

			e.printStackTrace();
		}

	}

	void checkAmt(int tdate) {
		try {
			stmt = c.con.prepareStatement(
					"select sum(amt),acno from transaction where tdate=? and type = 'C' group by acno");
			stmt.setInt(1, tdate);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				if (rs.getInt(1) >= 100000) {
					System.out.println("Suspicious Ac Spotted Ac no : " + rs.getLong(2));
					System.out.println("for being deposited by  : " + rs.getInt(1) + " Rupees on " + tdate);
				}
			}

		} catch (SQLException e) {

			e.printStackTrace();
		}

	}

	void checkMode(int tdate) {
		try {
			stmt = c.con.prepareStatement(
					"select count(amt),acno from transaction where tdate=? and type = 'C' group by acno");
			stmt.setInt(1, tdate);
			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				PreparedStatement pr = c.con.prepareStatement(
						"select count(amt),acno from transaction where tdate= ? and type = 'C' and fromac = 'Cash' and acno = ?");
				pr.setInt(1, tdate);
				pr.setLong(2, rs.getLong(2));
				ResultSet rs2 = pr.executeQuery();
				while (rs2.next()) {
					if (rs2.getInt(1)>1) {
						
					
					if (rs2.getInt(1) * 2 > rs.getInt(1)) {
						System.out.println("Suspicious Ac Spotted Ac no : " + rs.getLong(2));
						System.out.println(
								"for being deposited  : " + rs2.getInt(1) + " times on as cash on" + tdate);
					}}
				}

			}

		} catch (SQLException e) {

			e.printStackTrace();
		}

	}

}
