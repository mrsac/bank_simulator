package com.attinad.project1.bank;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

public class PinValidate {
	public boolean pinTrue(long acno) {
		SqlCon c = new SqlCon();
		boolean truth = false;
		int count = 3;
		System.out.println("Enter your PIN");
		while (count >= 0) {
			Scanner sc = new Scanner(System.in);
			int pin = sc.nextInt();
			try {
				PreparedStatement stmt = c.con.prepareStatement(
						"select * from user_info join ac_info on user_info.userid = ac_info.userid where ac_no= ? and pass = ?");
				stmt.setLong(1, acno);
				stmt.setInt(2, pin);
				ResultSet rs = stmt.executeQuery();
				if (rs.next())
					truth = true;

			} catch (SQLException e) {

				e.printStackTrace();
			}
			if (truth)
				break;
			else
				System.out.println("Incorrect PIN enter again");
			count--;
		}

		return truth;

	}

	public boolean pinTrue(String name) {
		SqlCon c = new SqlCon();
		boolean truth = false;
		int count = 3;
		System.out.println("Enter your PIN");
		while (count >= 0) {
			Scanner sc = new Scanner(System.in);
			int pin = sc.nextInt();
			try {
				PreparedStatement stmt = c.con.prepareStatement(
						"select * from user_info join ac_info on user_info.userid = ac_info.userid where name= ? and pass = ?");
				stmt.setString(1, name);
				stmt.setInt(2, pin);
				ResultSet rs = stmt.executeQuery();
				if (rs.next())
					truth = true;

			} catch (SQLException e) {

				e.printStackTrace();
			}
			if (truth == true) {
				break;
			} else {
				System.out.println("Incorrect PIN enter again");

			}
			count--;
		}

		return truth;

	}

	public boolean pinTrue(int userid) {
		SqlCon c = new SqlCon();
		boolean truth = false;
		int count = 3;
		System.out.println("Enter your PIN");
		while (count >= 0) {

			Scanner sc = new Scanner(System.in);
			int pin = sc.nextInt();
			try {
				PreparedStatement stmt = c.con.prepareStatement(
						"select * from user_info join ac_info on user_info.userid = ac_info.userid where userid= ? and pass = ?");
				stmt.setInt(1, userid);
				stmt.setInt(2, pin);
				ResultSet rs = stmt.executeQuery();
				if (rs.next())
					truth = true;

			} catch (SQLException e) {

				e.printStackTrace();
			}
			if (truth == true) {
				break;
			} else {
				System.out.println("Incorrect PIN enter again");

			}
			count--;
		}
		return truth;

	}

}
