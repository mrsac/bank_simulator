package com.attinad.project1.bank;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;

public class AddAcc {
	long deposit;
	public long acno;
	public void createAc(int  userid) {
		System.out.println("Creating an Account, Enter your initial Deposit");
		Scanner sc = new Scanner(System.in);
		deposit =  sc.nextLong();
		while(deposit<1000)
		{
			System.out.println("Enter Amount Greater than 1k");
			deposit = sc.nextLong();
		}
		System.out.println("Enter The type of AC 1.Savings 2.Recurring");
		int opt= sc.nextInt();
		String type;
		if(opt ==2)
			type = "Recurring";
		else 
			type ="Savings";
		
		SqlCon c = new SqlCon();
		try {
			PreparedStatement stmt=c.con.prepareStatement("insert into ac_info (userid,type,balance) values(?,?,?)");
			stmt.setInt(1, userid);
			stmt.setString(2, type);
			stmt.setDouble(3, 0);
			int i=stmt.executeUpdate();
			Transaction t = new Crediting();
			GetAcno g = new GetAcno();
			acno= g.getAc(userid);
			t.executeTrans(deposit, acno, "Cash" );
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

}
