package com.attinad.project1.bank;

import java.sql.SQLException;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// Welcome w = new Welcome();
		// AddUser u = new AddUser();
		// Controller.value=1;
		SqlCon c = new SqlCon();
		Scanner sc = new Scanner(System.in);
		AddUser adduser;
		Transaction t;
		FundTransfer ft;
		ViewDetails vd;
		SuperUser su;
		
		
		int opt = 0;
		System.out.println("Welcome to Freeworld's Bank");
		while (opt != 9) {
			System.out.println("1.CreateUser\n2.Credit\n3.Debit\n"
					+ "4.Transfer\n5.Search by Name\n6.Search By Acno\n7.Search By Cid\n8.SuperUser\n9.Quit");
			opt = sc.nextInt();
			switch (opt) {
			case 1:
				adduser = new AddUser();
				adduser.createUser();
				break;
			case 2:
				t = new Crediting();
				System.out.println("Enter Amount AccountNo and depositerAc/mode(Cash/Cheque))");
				t.executeTrans(sc.nextLong(), sc.nextLong(), sc.next());
				break;
			case 3:
				t = new Debiting();
				System.out.println("Enter Amount AccountNo and BeneficiaryAc/mode(Cash/Cheque))");
				t.executeTrans(sc.nextLong(), sc.nextLong(), sc.next());
				break;
			case 4:
				ft = new FundTransfer();
				System.out.println("Enter Debiter's AcNo, Creditor's AcNo, Amount");
				ft.transferAmt(sc.nextLong(), sc.nextLong(), sc.nextLong());
				break;
			case 5:
				System.out.println("Enter The AC Holder's first name as in PAN card");
				vd = new ViewDetails();
				vd.viewDetails(sc.next());
				break;
			case 6:
				System.out.println("Enter The Account Number");
				vd = new ViewDetails();
				vd.viewDetails(sc.nextLong());
				break;
			case 7:
				System.out.println("Enter The USER_ID");
				vd = new ViewDetails();
				vd.viewDetails(sc.nextInt());
				break;
			case 8:
				su= new SuperUser();
				su.walkThrough();
				

			case 9:
				System.out.println("ThankYou");
				break;

			default:
				System.out.println("Incorrect Input");

			}

		}

		try {
			c.con.close();
		} catch (SQLException e) {

			e.printStackTrace();
		}
		Comment com = new Comment();
		com.enterComment();


	}

}
