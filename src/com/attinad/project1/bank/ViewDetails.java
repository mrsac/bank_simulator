package com.attinad.project1.bank;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ViewDetails {
	SqlCon c = new SqlCon();
	PinValidate pv = new PinValidate();
	public void viewDetails(String name) {
		try {
			if(pv.pinTrue(name)) {
			PreparedStatement stmt = c.con.prepareStatement("select name, fname, email, pan ,type , balance, ac_no from user_info join ac_info on user_info.userid = ac_info.userid where user_info.name = ?");
			stmt.setString(1, name);
			ResultSet rs = stmt.executeQuery();
			while(rs.next())
			{
				System.out.println(rs.getString(1)+"\nSon of- "+rs.getString(2)+"\nEmail -"+rs.getString(3)+"\nPan -"+rs.getString(4)+"\nAC type- "+rs.getString(5)+"\nAC Bal = Rs"+rs.getLong(6)+"\nIn account : "+rs.getLong(7));
				miniStatement(rs.getLong(7));
			}

			}
			else
			{
				System.out.println("Failed due to Incorrect PIN");
				System.exit(0);
			}
		} catch (SQLException e) {
			
			e.printStackTrace();
			

		}
		
		
		
		
	}
	public void viewDetails(int cid) {
		long acno=0;try {
			if(pv.pinTrue(cid)) {
			PreparedStatement stmt = c.con.prepareStatement("select name, fname, email, pan ,type , balance, ac_no from user_info join ac_info on user_info.userid = ac_info.userid where user_info.userid = ?");
			stmt.setInt(1, cid);
			ResultSet rs = stmt.executeQuery();
			while(rs.next())
			{
				System.out.println(rs.getString(1)+"\nSon of- "+rs.getString(2)+"\nEmail -"+rs.getString(3)+"\nPan -"+rs.getString(4)+"\nAC type- "+rs.getString(5)+"\nAC Bal = Rs"+rs.getLong(6)+"\nIn account : "+rs.getLong(7));
				acno = rs.getLong(7);
			
			}
			}
			else
			{
				System.out.println("Failed due to Incorrect PIN");
				System.exit(0);
			}
		
		} catch (SQLException e) {
			
			e.printStackTrace();
			

		}
		miniStatement(acno);
		
	}
	public void viewDetails(Long acno) {
		try {
			if(pv.pinTrue(acno)) {
			PreparedStatement stmt = c.con.prepareStatement("select name, fname, email, pan ,type , balance, ac_no from user_info join ac_info on user_info.userid = ac_info.userid where ac_info.ac_no = ?");
			stmt.setLong(1, acno);
			ResultSet rs = stmt.executeQuery();
			while(rs.next())
			{
				System.out.println(rs.getString(1)+"\nSon of- "+rs.getString(2)+"\nEmail -"+rs.getString(3)+"\nPan -"+rs.getString(4)+"\nAC type- "+rs.getString(5)+"\nAC Bal = Rs"+rs.getLong(6)+"\nIn account : "+rs.getLong(7));
			}
			}
			else
			{
				System.out.println("Failed due to Incorrect PIN");
				System.exit(0);
			}
			
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		miniStatement(acno);
		
	}
	public void miniStatement(long acno) {
		try {
			PreparedStatement stmt = c.con.prepareStatement("select * from transaction where acno = ?");
			stmt.setLong(1, acno);
			ResultSet rs = stmt.executeQuery();
			System.out.println("Viewing the MiniStatement of "+acno);
			System.out.println("   Amt Type Format  Date   Trans_Id");
			while(rs.next()) {
				System.out.println("  "+rs.getLong(2)+"    "+rs.getString(3)+"  "+rs.getString(4)+"  "+rs.getInt(5)+"  "+rs.getInt(6));
			}
			
		}catch(Exception e) {
			e.printStackTrace();
		}
	}

}
