package com.attinad.project1.bank;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class GetAcno {
	public long getAc(int userid) {
		ResultSet rs;
		long x = 0;
		SqlCon c = new SqlCon();
		PreparedStatement stmt;
		try {
			stmt = c.con.prepareStatement("select ac_no from ac_info where userid=?");

			stmt.setInt(1, userid);
			rs = stmt.executeQuery();
			while (rs.next()) {
				x = rs.getLong(1);
				System.out.println("Your Ac no = " + x);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return x;
	}
}
