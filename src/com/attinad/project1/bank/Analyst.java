package com.attinad.project1.bank;

import java.util.Scanner;

public class Analyst {
	public void performAnalysis() {

		{	Scanner sc = new Scanner(System.in);
			int tdate;
			System.out.println("Showing Analysis Report\n\n");
			System.out.println("Enter the date since this function is in beta");
			tdate = sc.nextInt();
			Report report = new Report();
			report.dayCheck(tdate);
			report.checkAmt(tdate);
			report.checkMode(tdate);

		}

	}

}
