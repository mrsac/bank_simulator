package com.attinad.project1.bank;

import java.sql.Connection;
import java.sql.DriverManager;

public class SqlCon {
	public Connection con;

	public SqlCon() {
		try {
			Class.forName("com.mysql.jdbc.Driver");

			con = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/bank", "root", "root");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
