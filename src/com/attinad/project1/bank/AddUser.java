package com.attinad.project1.bank;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

public class AddUser {
	SqlCon c = new SqlCon();
	int userid;

	public void createUser() {

		String name, email, pan, fname;
		int pin;
		System.out.println("Enter Details");
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Your Name");
		name = sc.next();
		System.out.println("Enter Father's Name");
		fname = sc.next();
		System.out.println("Enter valid email id");
		Email echeck = new Email();
		email = sc.next();
		while (!echeck.isValidEmailAddress(email)) {
			System.out.println("Enter a valid email");
			email = sc.next();

		}
		System.out.println("Enter PAN Number");
		pan = sc.next();
		while (pan.length() != 10) {
			System.out.println("Enter a valid pan");
			pan = sc.next();

		}
		pin = sc.nextInt();
		while (String.valueOf(pin).length() != 4) {
			System.out.println("Enter a valid pin");
			pan = sc.next();
		}

		try {
			PreparedStatement stmt = c.con
					.prepareStatement("insert into user_info (name,fname,email,pan,pass) values(?,?,?,?,?)");
			stmt.setString(1, name);
			stmt.setString(2, fname);
			stmt.setString(3, email);
			stmt.setString(4, pan);
			stmt.setInt(5, pin);
			int i = stmt.executeUpdate();
			stmt = c.con.prepareStatement("select userid from user_info where pan = ?");
			stmt.setString(1, pan);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				userid = rs.getInt(1);
			}
			AddAcc ac = new AddAcc();
			ac.createAc(userid);
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}
}