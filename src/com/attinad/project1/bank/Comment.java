package com.attinad.project1.bank;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

public class Comment {
	SqlCon c = new SqlCon();
	public  void enterComment(){
		try {
			System.out.println("Please help us serve you better by leaving an anonymous feedback/suggestion ");
			Scanner sc = new Scanner(System.in);
			String remark="",temp=" ";
			while(!temp.equals(".")) {
				temp = sc.next();
			remark= remark+" "+temp;}
			PreparedStatement stmt = c.con.prepareStatement("insert into comments values(?)");
			stmt.setString(1, remark);
			int i = stmt.executeUpdate();
			if(i>0)
				System.out.println("Your feedback has been recorded\nThankYou");
				
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
	}
	public void viewComment(){
		
		try {
			PreparedStatement stmt = c.con.prepareStatement("select * from comments");
			ResultSet rs  = stmt.executeQuery();
			System.out.println("Comments\n\n");
			while(rs.next()) {
				System.out.println(rs.getString(1));
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("\n\n");
	}

}
